Описание предметной области.
Система “SalonBeaty” предназначена для салона красоты, а именно для администратора заведения, чтобы управлять записями на прием, отслеживать постоянных клиентов.
Ссылка на тз:
https://gitlab.com/AnaXianA/beatysalonaksutova/-/blob/main/TZ.docx?ref_type=heads

ERD-диаграмма
![alt text](бд.png)

Основные сущности диаграммы:
Client-Сущность клиенты, включающая в себе информацию о клиентах
Worker-Сущность работники, включающая в себе информацию о работниках
Service-Сущность услуги, включащая в себя информацию о услугах
Specialization- сущность специализация (работнкиов)
Check- чек, включает итоговую сумму
WorkingShift-сущность рабочей смены работников

Use Case диаграмма:
![alt text](usecase.png)

Последовательная диаграмма:
![alt text](Последовательная_диаграмма.png)

Дизайн прложения:
https://www.figma.com/file/TF7ZB1kANX6RlQsgl5Pfui/Untitled?type=design&mode=design&t=1G6wZsLlABertLtw-0

Авторизация:
![image](1.png)
Клиенты:
![image](2.png)
Услуги:
![image](3.png)
Продукты:
![image](4.png)
Добавление в корзину:
![image](5.png)
Корзина:
![image](6.png)
![image](7.png)
